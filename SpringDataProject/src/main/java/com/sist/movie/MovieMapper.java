package com.sist.movie;

import org.apache.ibatis.annotations.Insert;
/*
 *  private int cateno;
   private int mno;
   private String title;
   private String poster;
   private String regdate;
   private String grade;
   private String genre;
   private String score;
   private String story;
   private String actor;
   private String director;
 */
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.*;
public interface MovieMapper {
   @Insert("INSERT INTO springMovie VALUES("
		  +"#{cateno},(SELECT NVL(MAX(mno)+1,1) FROM springMovie),"
		  +"#{title},#{poster},#{regdate},#{grade},#{genre},#{score},"
		  +"#{story},#{actor},#{director})")
   public void springMovieInsert(MovieVO vo);
   
   @Insert("INSERT INTO springBoxOffice VALUES("
			  +"#{cateno},(SELECT NVL(MAX(mno)+1,1) FROM springBoxOffice),"
			  +"#{title},#{poster},#{regdate},#{grade},#{genre},#{score},"
			  +"#{story},#{actor},#{director})")
   public void springBoxOfficeInsert(MovieVO vo);
   
   @Select("SELECT mno,title FROM springMovie")
   public List<MovieVO> movieAllData();
   
   @Update("UPDATE springMovie SET "
		  +"key=#{key} "
		  +"WHERE mno=#{mno}")
   public void movieUpdate(MovieVO vo);
}










