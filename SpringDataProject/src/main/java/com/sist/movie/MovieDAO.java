package com.sist.movie;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class MovieDAO {
   @Autowired
   private MovieMapper mapper;
   
   public void springMovieInsert(MovieVO vo)
   {
	   mapper.springMovieInsert(vo);
   }
   
   public void springBoxOfficeInsert(MovieVO vo)
   {
	   mapper.springBoxOfficeInsert(vo);
   }
   public List<MovieVO> movieAllData()
   {
	   return mapper.movieAllData();
   }
   public void movieUpdate(MovieVO vo)
   {
	   mapper.movieUpdate(vo);
   }
}
