package com.sist.getpost;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.*;
import com.sist.dao.*;
// 데이터 전송 방식 => GET @GetMapping,POST @PostMapping  ==> @GetMapping+@PostMapping=@RequestMapping
// @RequestMapping(value="",Method=GET)
import java.io.*;
@Controller
public class EmpController {
   @Autowired
   private EmpDAO dao;
   
   @GetMapping("emp/list.do")
   public String emp_list(Model model)
   {
	   List<EmpVO> list=dao.empListData();
	   model.addAttribute("list", list);
	   
	   List<EmpVO> eList=dao.empAllData();
	   String data="empno,ename,job,sal\n";
	   for(EmpVO vo:eList)
	   {
		   data+=vo.getEmpno()+","+vo.getEname()+","+vo.getJob()+","+vo.getSal()+"\n";
	   }
	   
	   data=data.substring(0,data.lastIndexOf("\n"));
	   
	   try
	   {
		   FileWriter fw=new FileWriter("c:\\upload\\emp.csv");
		   fw.write(data);
		   fw.close();
	   }catch(Exception ex){}
	   
	   return "emp/list";
   }
   
   @GetMapping("emp/detail.do")
   public String emp_detail(int empno,Model model)
   {
	   EmpVO vo=dao.empDetailData(empno);
	   model.addAttribute("vo", vo);
	   return "emp/detail";
   }
   
   @PostMapping("emp/detail.do")
   public String emp_detail2(int empno,Model model)
   {
	   EmpVO vo=dao.empDetailData(empno);
	   model.addAttribute("vo", vo);
	   return "emp/detail";
   }
}











