import React, {Component} from 'react';

/**
 * funciton : 함수형 => 한개의 기능
 *  <InputTest name="aaa" />
 *  function InputTest(name) {
 *      return (
 *
 *      );
 *  }
 * class : 클래스형 => 여러개 기능을 합쳐서 처리
 *
 *
 * */

class InputTest extends Component {
    constructor(props) {
        super(props);
        this.state = {
            value: ''
        };
        this.dataChange = this.dataChange.bind(this);
        this.btnClick = this.btnClick.bind(this);
    }

    dataChange(e) {
        this.setState({value:e.target.value});
    }

    btnClick(e) {
        // this.setState({value:e.target.value});
    }

    render() {
        return (
            <div>
                입력:<input type={"text"} size={"15"} className={"input-sm"} onChange={this.dataChange} />
                <input type={"button"} value={"전송"} className={"btn btn-sm btn-success"} onClick={this.btnClick} />
                <br2/>
                <h2>{this.state.value}</h2>
            </div>
        );
    }

}

export default InputTest;