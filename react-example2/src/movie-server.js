/**
 * mongodb 설정
 * express 설정
 * */
// import MongoClient from "mongodb"
// import * as express from "express";
// const Client = MongoClient;
// const app = express();
const Client = require('mongodb').MongoClient;
const express = require('express');
const app = express();
// port 번호 0 ~ 65535 => (0 ~ 1024), 7000,8080 알려진 포트들
const port = 3355;
const request = require('request');
const xml2js = require('xml2js');

app.listen(port, () => {
    console.log('Server Start!!', 'http://localhost:3355');
});

app.all('/*', function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "X-Requested-With");
    next();
});

app.get('/movie', (req, res) => {
    // MongoDB 데이터 읽기
    const url = 'mongodb://211.238.142.181:27017';
    const page = req.query.page;
    const rowSize = 8;
    const skip = rowSize * page - rowSize;

    /*Client.connect(url, (err, client) => {
        const db = client.db('mydb');
        db.collection('movie').find({}).skip(skip).limit(rowSize).toArray((err, docs) => {
            res.json(docs);
            client.close();
        });
    });*/
    Client.connect(url, (err, client) => {
        const db = client.db('mydb');
        db.collection('movie').find({}).skip(skip).limit(rowSize).toArray((err, docs) => {
            res.json(docs);
            client.close();
        });
    });
});

app.get('/news', (req, res) => {
    const fd = encodeURIComponent(req.query.fd);
    console.log("fd="+fd);
    const url = 'http://newssearch.naver.com/search.naver?where=rss&query='+fd;

    /**
     * 서버 => 전송값
     * 1) axios
     * 2) fetch
     * 3) request
     * */

    const parser = xml2js.Parser();
    request({url:url},(err,request,xml) => {
        parser.parseString(xml,(err,pJson) => {
            let item = pJson.rss.channel[0].item;
            // console.log(item);
            res.json(item);
        });
    })
});
