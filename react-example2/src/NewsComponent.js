import React, {Component} from 'react';
import Axios from 'axios';

class NewsComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            url: 'http://localhost:3355/news',
            news: [],
            fd: '영화',
        };
        // 입력데이터 저장
        this.changeHandler = this.changeHandler.bind(this);
        // 검색 버튼
        this.findClick = this.findClick.bind(this);
    }

    changeHandler(e) {
        this.setState({fd: e.target.value});
    }

    findClick() {
        this.getNews();
    }

    getNews() {
        Axios.get(this.state.url, {
            params: {
                fd: this.state.fd
            }
        }).then((response) => {
            console.log(response);
            this.setState({news: response.data});
        });
    }

    // 데이터 읽기
    componentWillMount() {
        this.getNews();
    }

    render() {
        const html = this.state.news.map((m, index) =>
            <table className={"table table-bordered"} key={index}>
                <tbody>
                <tr>
                    <td className={"text-left"} style={{"backgroundColor":"#ccccff","fontSize":"15px"}}>
                        {m.title}
                    </td>
                    <td className={"text-right"}>
                        {m.author}
                    </td>
                </tr>
                <tr>
                    <td colSpan={"2"}>
                        <a href={m.link}>{m.description}</a>
                    </td>
                </tr>
                </tbody>
            </table>
        );

        return (
            <div className={"row"} style={{"width": "800px", "margin": "0px auto"}}>
                <table className={"table"}>
                    <tr>
                        <td>
                            검색 : <input type={"text"} className={"input-sm"} size={"30"} onChange={this.changeHandler}/>
                            <input type={"button"} value={"찾기"} onClick={this.findClick}/>
                        </td>
                    </tr>
                </table>
                <table className={"table"}>
                    <tbody>
                    <tr>
                        <td>{html}</td>
                    </tr>
                    </tbody>
                </table>
            </div>
        );
    }
}

export default NewsComponent;