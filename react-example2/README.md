##React
1일차 복습
1.UI 라이브러리
2.JSX => javascript + XML
3.단방향 통신
4.속도최적화 => 가상 DOM

##UI 라이브러리
동작 => 리액트 생명주기
1)	생성(컴포넌트 => class)
	constructor => 생성자
					속성값전달, 변수 선언, 이벤트 등록
	준비상태 => 외부 데이터를 저장(Node.js)
	componentWillMount => 데이터 저장
	화면 출력 => render() => HTML을 제작 => 데이터를 출력
	componentDidMount => 실제 DOM 반영

##JSX
1)	계층구조 => root태그 한개만 존재
2)	속성값 => ""
3)	여는태그, 닫는 태그일치
	단독태그 잘쓰기
	
##단방향 통신
-> 상위 계층을 통하여 데이터 전달해야됨
-> redux saga 보완

화면 출력 => 데이터 수집
이벤트 처리 handler

화면이동 router
컴포넌트 개발 => webpack