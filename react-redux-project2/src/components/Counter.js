import React, {Component} from 'react';

/**
 * store => action, state
 */
class Counter extends Component {
    render() {
        return (
            <div>
                <h1>{this.props.store.getState().counter.value}</h1>
            </div>
        );
    }
}

export default Counter;