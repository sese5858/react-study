import React, {Component} from 'react';
import {setDiff} from "../actions";

/**
 * store (action 정보, 데이터 state)
 * ==> 이벤트 발생 => dispatch() => store 전송 => state 변경
 */
class Option extends Component {
    constructor(props) {
        super(props);
        this.onChange = this.onChange.bind(this);
    }

    /*
        1. 이벤트 발생 => onChange
        2. 처리요청
            this.props.store.dispatch(setDiff(e.target.value))
        3. setDiff() : action
        4. reducer => store =>state
        5. render()
     */
    onChange(e) {
        this.props.store.dispatch(setDiff(Number(e.target.value)));
    }

    render() {
        return (
            <div>
                <input type={"text"} onChange={this.onChange} value={this.props.store.getState().counter.diff}/>
            </div>
        );
    }
}

export default Option;