import React, {Component} from 'react';
import './App.css';
import Counter from "./components/Counter";
import Option from "./components/Option";
import Button from "./components/Button";

/**
 * Redux 데이터 관리 라이브러리
 * 스토어(store) : 컴포넌트가 사용할 데이터 내장 (state) 상태관리
 * 액션(action) : 데이터를 변경할때 참조하는 객체
 * 디스패치(dispatch) : 액션을 스토어에 전달 => 데이터 변경
 * 리듀서(reducer) : 데이터 실제 변경
 * 구독(subscribe) : 스토어의 존재하는 값을 컴포넌트가 읽음
 *
 * Components event 발생
 *  => reducer(state 갱신)
 *  => this.props.stored.dispatch
 *  => store.subscribe
 *
 */
class App extends Component {
    render() {
        return (
            <div>
                <Counter store={this.props.store}/>
                <Option store={this.props.store}/>
                <Button store={this.props.store}/>
            </div>
        );
    }
}

export default App;
