import React, {Component} from "react";

/*
    new App2() => <App2 />
    
    => App2
    1) constructor() : 생성자 호출 => 메모리에 저장
    2) componentWillMount() =>
    3) render() => XML => HTML로 변환
    4) componentDidMount() => 완료 (window.onload)
    =============================================== 컴포넌트 실행

    => HTML에 innerHTML 수행
    
    사용법
    JSX => javascript + XML => XML 파싱
    ===
    1) 최상위 root 필요 (계층구조)
        <div>aaa</div>
        <div>bbb</div>  => error => DOM

        <App2 name="홍길동" />
 */
class App2 extends Component {

    // 서버에서 들어오는 값 (외부)
    componentWillMount() {
    }

    // 속성값 전달, 변수선언, 이벤트 등록
    constructor(props) {
        // 속성값 읽기
        super(props);
    }

    // 받은값 출력 => HTML 출력
    render() {
        // JSX
        /*
            1) 최상위 태그 필요
            2) 태그는 소문자(대소문자 구분)
                => 예외) 사용자 정의 태그는 대문자 시작
            3) 여는태그 닫는 태그가 동일
                <a><b><c></b></c></a> => 에러
            4) 속성값
                <a href=""> => ""를 사용
            5) 외부 css => <a class=""> => <a className="">  으로 사용
            5-1) 내부 css => <a style={{font-size:"10px"}}> => <a style={{fontSize:"10px"}}>
            6) 데이터가 많은경우
                =============== 루프 (for) => map() ES6
            7) 람다식 (화살표함수) ES6

         */

        return (
            /*React.createElement("ol", null, React.createElement("li", null, "홍길동"),
                React.createElement("li", null, "박은수"),
                React.createElement("li", null, "심청이")
            )*/
            <div>
                <ol>
                    <li>홍길동</li>
                    <li>박은수</li>
                    <li>심청이</li>
                </ol>
                <ul>
                    <li>홍길동</li>
                    <li>박은수</li>
                    <li>심청이</li>
                </ul>
            </div>
        )
    }

    // onload => Jquery, 서버 종료
    componentDidMount() {
    }
}

export default App2;

