import React, {Component} from 'react';
import $ from "jquery";
import io from 'socket.io-client';

const serverSocket = io.connect('http://localhost:3001');

class ChatMain extends Component {
    constructor(props) {
        super(props);
        this.state = {
            logs: []
        }
    }

    render() {
        const html = this.state.logs.map((m, index) =>
            <div className={"message_right"} key={index}>
                <div className={"message-text"}>{m.message}</div>
            </div>
        );

        return (
            <div id={"chat_container"}>
                <div id="chat" className='active'>
                    <header><h1>Chat</h1></header>
                    <section className="content">
                        <div className="message_content">
                            {html}
                        </div>
                    </section>
                    <ChatForm/>
                </div>
            </div>
        );
    }

    componentDidMount() {
        $('div#chat').toggleClass('active');
        const $win = $(window);
        const top = $(window).scrollTop(); // 현재 스크롤바의 위치값을 반환합니다.

        /*사용자 설정 값 시작*/
        const speed = 1000;     // 따라다닐 속도 : "slow", "normal", or "fast" or numeric(단위:msec)
        const easing = 'linear'; // 따라다니는 방법 기본 두가지 linear, swing
        const $layer = $('div#chat_container'); // 레이어 셀렉팅
        const layerTopOffset = 0;   // 레이어 높이 상한선, 단위:px
        $layer.css('position', 'absolute');
        /*사용자 설정 값 끝*/

        // 스크롤 바를 내린 상태에서 리프레시 했을 경우를 위해
        if (top > 0)
            $win.scrollTop(layerTopOffset + top);
        else
            $win.scrollTop(0);

        //스크롤이벤트가 발생하면
        $(window).scroll(function () {
            let yPosition = $win.scrollTop() + 300;
            if (yPosition < 0) {
                yPosition = $win.scrollTop() + 300;
            }
            $layer.animate({"top": yPosition}, {duration: speed, easing: easing, queue: false});
        });

        serverSocket.on('chat-msg', (obj) => {
            const log2 = this.state.logs;
            log2.push(obj);
            this.setState({logs: log2});
        });
    }
}

class ChatForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            message: ''
        }
    }

    messageChange(e) {
        this.setState({message: e.target.value});
    }

    send(e) {
        // 전송 emit 받기 on
        if (e.key === 'Enter') {
            e.preventDefault();
            serverSocket.emit('chat-msg', {
                message: this.state.message
            });
            e.target.value = '';
            this.setState({message: ''});
        }
    }

    render() {
        return (
            <form action="">
                <input id="input_chat" type="text" onChange={this.messageChange.bind(this)}
                       onKeyPress={this.send.bind(this)}/>
            </form>
        );
    }
}

export default ChatMain;