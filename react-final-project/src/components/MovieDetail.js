import React, {Component} from 'react';
import Axios from "axios";

class MovieDetail extends Component {
    constructor(props) {
        super(props);
        this.state = {
            movie_data: {},
            serverUrl: 'http://localhost:3355'
        }
    }

    componentWillMount() {
        Axios.get(this.state.serverUrl + '/movie_detail',
            {
                params: {
                    mno: this.props.match.params.mno,
                    flag: this.props.match.params.flag
                }
            }
        ).then((response) => {
            this.setState({movie_data: response.data});
        });
    }

    render() {
        return (
            <div className="container text-center bg-grey">
                <h2>영화 상세 보기</h2><br/>
                <div className="row text-center">
                    <table className={"table"}>
                        <tbody>
                        <tr>
                            <td>
                                <embed src={"https://www.youtube.com/v/" + this.state.movie_data.key + "?version=3"}
                                       width={"100%"} height={"400px"} allowscriptaccess={"always"}
                                       type={"application/x-shockwave-flash"}
                                       allowFullScreen={"true"}/>
                                {/*<div className='embed-container'>
                                        <iframe src={'https://www.youtube.com/embed/' + this.state.movie_data.key}
                                                frameBorder={'0'}
                                                allowFullScreen/>
                                    </div>*/}
                            </td>
                        </tr>
                        </tbody>
                    </table>
                    <table className={"table"}>
                        <tbody>
                        <tr>
                            <td width={"30%"} rowSpan={"6"} className={"text-center"}>
                                <img src={this.state.movie_data.poster} alt={this.state.movie_data.title}/>
                            </td>
                            <td colSpan={"2"}>
                                {this.state.movie_data.title}
                            </td>
                        </tr>
                        <tr>
                            <td width={"20%"} className={"text-left"}>감독</td>
                            <td width={"50%"} className={"text-right"}>{this.state.movie_data.director}</td>
                        </tr>
                        <tr>
                            <td width={"20%"} className={"text-left"}>장르</td>
                            <td width={"50%"} className={"text-right"}>{this.state.movie_data.genre}</td>
                        </tr>
                        <tr>
                            <td width={"20%"} className={"text-left"}>등급</td>
                            <td width={"50%"} className={"text-right"}>{this.state.movie_data.grade}</td>
                        </tr>
                        <tr>
                            <td width={"20%"} className={"text-left"}>개봉일</td>
                            <td width={"50%"} className={"text-right"}>{this.state.movie_data.regdate}</td>
                        </tr>
                        <tr>
                            <td colSpan={"3"} ><p style={{"maxHeight": "120px"}}>{this.state.movie_data.story}</p></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <br/>
            </div>
        );
    }
}

export default MovieDetail;