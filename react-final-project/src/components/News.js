import React, {Component} from 'react';
import Axios from 'axios';

class News extends Component {
    constructor(props) {
        super(props);
        this.state = {
            news_data: [],
            serverUrl: 'http://localhost:3355'
        }
    }

    componentWillMount() {
        Axios.get(this.state.serverUrl + '/movie_news').then((response) => {
            this.setState({news_data: response.data});
        });
    }

    render() {
        const html = this.state.news_data.map((m) =>
            <table className={"table"} key={m.no}>
                <tbody>
                <tr>
                    <td className={"text-center"} width={"30%"} rowSpan={"3"}>
                        <img src={m.poster} alt={m.title} width={"100%"}/>
                    </td>
                    <td className={"text-left"} width={"70%"}>
                        <a href={m.link}>{m.title}</a>
                    </td>
                </tr>
                <tr>
                    <td>{m.content}</td>
                </tr>
                <tr>
                    <td className={"text-right"}>{m.author}</td>
                </tr>
                </tbody>
            </table>
        );
        return (
            <div className="container text-center bg-grey">
                <h2>영화 뉴스</h2><br/>
                <div className="row text-center">
                    <table className={"table"}>
                        <tbody>
                        <tr>
                            <td>{html}</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <br/>
            </div>
        );
    }
}

export default News;