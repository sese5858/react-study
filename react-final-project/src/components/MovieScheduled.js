import React, {Component} from 'react';
import Axios from "axios";
import {NavLink} from 'react-router-dom';

class MovieScheduled extends Component {
    constructor(props) {
        super(props);
        this.state = {
            movie_data: [],
            page: 1,
            totalPage: 0,
            serverUrl: 'http://localhost:3355'
        }
    }

    componentWillMount() {
        const serverUrl = 'http://localhost:3355'

        Axios.get(serverUrl + '/movie_schedule',
            {
                params: {
                    page: this.state.page,
                }
            }
        ).then((response) => {
            this.setState({movie_data: response.data});
        });
    }

    render() {
        const html = this.state.movie_data.map((m) => (
            <div className="col-sm-3" key={m.mno}>
                <div className="thumbnail">
                    <NavLink to={"/detail/schedule/" + m.mno}><img src={m.poster} alt={m.title} width="400" height="300"/></NavLink>
                    <p><strong>{m.director}</strong></p>
                    <p>{m.title}</p>
                </div>
            </div>
        ));

        return (
            <div className="container-fluid text-center bg-grey">
                <h2>개봉 예정 영화</h2><br/>
                <div className="row text-center display-flex">
                    {html}
                </div>
                <br/>
            </div>
        );
    }
}

export default MovieScheduled;