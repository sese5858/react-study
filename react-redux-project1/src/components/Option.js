import React, {Component} from 'react';

class Option extends Component {
    constructor(props) {
        super(props);
        this.onChange = this.onChange.bind(this);
    }

    onChange(e) {
        this.props.onChange(e.target.value);
    }

    render() {
        return (
            <div>
                <input type={"text"} className={"input-sm"} onChange={this.onChange} value={this.props.diff}/>
            </div>
        );
    }
}

export default Option;