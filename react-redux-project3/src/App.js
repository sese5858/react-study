import React, {Component} from 'react';
import './App.css';
import Menu from "./component/Menu";
import MovieView from "./component/MovieView";

class App extends Component {
    render() {
        return (
            <div>
                <Menu store={this.props.store}/>
                <MovieView store={this.props.store}/>
            </div>
        );
    }
}

export default App;
