export const R_MOVIE = 'R_MOVIE';
export const S_MOVIE = 'S_MOVIE';
export const BOX_OFFICE = 'BOX_OFFICE';

export function rMovie() {
    return {
        type: R_MOVIE,
    };
}

export function sMovie() {
    return {
        type: S_MOVIE,
    }
}

export function boxOffice() {
    return {
        type: BOX_OFFICE
    }
}