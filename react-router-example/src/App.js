import React from 'react';
import './App.css';
import BoxOffice from "./BoxOffice";
import Header from "./Header";
import Reserve from "./Reserve";
import OnLine from "./OnLine";
import Seat from "./Seat";
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';

/**
 * Component : 기능 1개 => 여러 컴포넌트 => 사이트로
 * =============================한개로 모아서 처리 => index.js 세팅
 */
class App extends React.Component {
    render() {
        return (
            <Router>
                <div>
                    <Header/>
                    <Switch>
                        <Route exact path={"/"} component={BoxOffice}></Route>
                        <Route path={"/seat"} component={Seat}></Route>
                        <Route path={"/online"} component={OnLine}></Route>
                        <Route path={"/reserve"} component={Reserve}></Route>
                    </Switch>
                </div>
            </Router>
        );
    }
}

export default App;
