import React, {Component} from 'react';
import Axios from 'axios';

class BoxOffice extends Component {

    constructor(props) {
        super(props);

        this.state = {
            itemArray: [],
            item: {},
            showResults: false
        };
    }

    handleClick(movie_data) {
        this.setState({item: movie_data, showResults: true});
    }

    componentWillMount() {
        Axios.get('http://localhost:3355/movie_info', {
            params: {
                no: 1
            }
        }).then((res) => {
            this.setState({itemArray: res.data});
            return res.data[0];
        }).then((firstItem) => {
            // this.handleClick(firstItem);
        });
    }

    render() {
        const html = this.state.itemArray.map((m) =>
            <tr onClick={this.handleClick.bind(this, m)} key={m.movieCd}>
                <td className={"text-left"}>{m.movieNm}</td>
                <td className={"text-center"}>{m.salesAmt}</td>
                <td className={"text-center"}>{m.audiCnt}</td>
                <td className={"text-center"}>{m.rankInten}</td>
            </tr>
        );

        return (
            <div className={"row"}>
                <h3 className={"text-center col-sm-12"}>일별 박스오피스</h3>
                <div className={"col-sm-7"}>
                    {this.state.showResults ? <BoxOfficeDetail movie_data={this.state.item}/> : null}
                </div>
                <div className={"col-sm-5"}>
                    <table className={"table table-striped"}>
                        <thead>
                        <tr>
                            <th>영화명</th>
                            <th>매출액</th>
                            <th>관객수</th>
                            <th>증감율</th>
                        </tr>
                        </thead>
                        <tbody>
                        {html}
                        </tbody>
                    </table>
                </div>
            </div>
        );
    }

}

class BoxOfficeDetail extends Component {
    constructor(props) {
        super(props);

        this.state = {
            serverUrl: 'http://www.kobis.or.kr'
        }
    }

    render() {
        return (
            <table className={"table table-hover"}>
                <tbody>
                <tr>
                    <td width={"30%"} className={"text-center"} rowSpan={"8"}>
                        <img src={this.state.serverUrl + this.props.movie_data.thumbUrl} width={"100%"}/>
                    </td>
                    <td colSpan={"2"} width={"70%"}>
                        <b>{this.props.movie_data.movieNm}</b><br/>
                        <sub>{this.props.movie_data.movieNmEn}</sub>
                    </td>
                </tr>
                <tr>
                    <td width={"20%"} className={"text-right"}>
                        <b>개봉일</b>
                    </td>
                    <td width={"50%"} className={"text-left"}>
                        <sub>{this.props.movie_data.startDate}</sub>
                    </td>
                </tr>
                <tr>
                    <td width={"20%"} className={"text-right"}>
                        <b>제작상태</b>
                    </td>
                    <td width={"50%"} className={"text-left"}>
                        <sub>{this.props.movie_data.moviePrdtStat}</sub>
                    </td>
                </tr>
                <tr>
                    <td width={"20%"} className={"text-right"}>
                        <b>관람등급</b>
                    </td>
                    <td width={"50%"} className={"text-left"}>
                        <sub>{this.props.movie_data.watchGradeNm}</sub>
                    </td>
                </tr>
                <tr>
                    <td width={"20%"} className={"text-right"}>
                        <b>상영시간</b>
                    </td>
                    <td width={"50%"} className={"text-left"}>
                        <sub>{(this.props.movie_data.showTm) ? this.props.movie_data.showTm + "분" : "해당정보없음"}</sub>
                    </td>
                </tr>
                <tr>
                    <td width={"20%"} className={"text-right"}>
                        <b>제작국가</b>
                    </td>
                    <td width={"50%"} className={"text-left"}>
                        <sub>{this.props.movie_data.repNationCd}</sub>
                    </td>
                </tr>
                <tr>
                    <td width={"20%"} className={"text-right"}>
                        <b>감독</b>
                    </td>
                    <td width={"50%"} className={"text-left"}>
                        <sub>{this.props.movie_data.director}</sub>
                    </td>
                </tr>
                <tr>
                    <td width={"20%"} className={"text-right"}>
                        <b>장르</b>
                    </td>
                    <td width={"50%"} className={"text-left"}>
                        <sub>{this.props.movie_data.genre}</sub>
                    </td>
                </tr>
                </tbody>
            </table>
        );
    }
}

export default BoxOffice;